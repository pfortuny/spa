#!/usr/bin/perl
#!/usr/bin/perl -w
# udpqotd - UDP message server
use strict;
use IO::Socket;
my($sock, $oldmsg, $newmsg, $hisaddr, $hishost, $MAXLEN, $PORTNO);
$MAXLEN = 1024;
$PORTNO = 5151;
$sock = IO::Socket::INET->new(LocalPort => $PORTNO, Proto => 'udp')
  or die "socket: $@";
print "Awaiting UDP messages on port $PORTNO\n";
$oldmsg = "This is the starting message.";
$| = 1;
if (my $pid = fork) {
  # parent
  while (1) {
    sleep 10;
    system "pfctl -t allowed -T expire 10";
  }
} elsif (defined $pid) {
  #child
  while ($sock->recv($newmsg, $MAXLEN)) {
    my($port, $ipaddr) = sockaddr_in($sock->peername);
    $hishost = gethostbyaddr($ipaddr, AF_INET);
    my @IPaddr = unpack("CCCC",$ipaddr);
    # print "Client $hishost said ``$newmsg''\n";
    if ($newmsg =~ /^hola$/) {
      system "pfctl -t allowed -T add " . join('.',@IPaddr);
      print "IP ADDRESS: [@IPaddr] added to <allowed> table.\n";
    } else {
      print "Wrong trial from [@IPaddr]\n";
    }
  }
} else {
  die "Cannot fork: $!:";
}
die "recv: $!";
