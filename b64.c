#include"b64.h"


int b64decode(char *from, char *to){
        BIO *bio, *b64;
        int i;
        char *from_n;
        from_n = calloc(1, strlen(from)+2);
        strlcpy(from_n, from, strlen(from)+1);
        /* just in case */
        *(from_n + strlen(from_n)) = '\n';
        *(from_n + strlen(from_n)+1) = '\0';
        /* EOjic */
        b64 = BIO_new(BIO_f_base64());
        bio = BIO_new(BIO_s_mem());
        i = BIO_write(bio,from_n,strlen(from_n));
        bio = BIO_push(b64,bio);
        i = BIO_ctrl_pending(bio);
        i = BIO_read(bio,to,strlen(from_n)*3/4);
        BIO_flush(bio);
        BIO_set_mem_eof_return(bio, 0);
        return(i);

}


int b64encode(char *from, int flen, char *to, int tlen){
        char *data;
        BIO *b64, *bio;
        int i;
        b64 = BIO_new(BIO_f_base64());
        bio = BIO_new(BIO_s_mem());
        bio = BIO_push(b64,bio);
        i = BIO_write(bio,from,flen); 
        BIO_flush(bio);
        i = BIO_get_mem_data(bio,&data);
        memcpy(to,data,i); 
        BIO_free_all(bio);
        return(i);
}





