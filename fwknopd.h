#define ETHER_ADDR_LEN	6
#define SIZE_ETHERNET  14
#define SSH_MAX_PUBKEY_BYTES 1024

/*
 * IP header:
 * not exactly the same as in netinet/ip.h
 * but useful for us
 */
struct iphdr {
	u_char ip_vhl;		/* version << 4 | header length >> 2 */
	u_char ip_tos;		/* type of service */
	u_short ip_len;		/* total length */
	u_short ip_id;		/* identification */
	u_short ip_off;		/* fragment offset field */
#define IP_RF 0x8000		/* reserved fragment flag */
#define IP_DF 0x4000		/* dont fragment flag */
#define IP_MF 0x2000		/* more fragments flag */
#define IP_OFFMASK 0x1fff	/* mask for fragmenting bits */
	u_char ip_ttl;		/* time to live */
	u_char ip_p;		/* protocol */
	u_short ip_sum;		/* checksum */
	struct in_addr ip_src,ip_dst; /* source and dest address */
};

#define IP_HL(ip)		(((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)		(((ip)->ip_vhl) >> 4)

/*
 * list of addresses (allowed ones)
 */
struct in_addr_item {
	struct in_addr address;
	struct in_addr_item *next;
};
struct in_addr_item *valid_sources;

#define IFNAMSIZ 16
#define PF_RULESET_NAME_SIZE 16
struct pfloghdr {
	u_int8_t        length;
	sa_family_t     af;
	u_int8_t        action;
	u_int8_t        reason;
	char            ifname[IFNAMSIZ];
	char            ruleset[PF_RULESET_NAME_SIZE];
	u_int32_t       rulenr;
	u_int32_t       subrulenr;
	uid_t           uid;
	pid_t           pid;
	uid_t           rule_uid;
	pid_t           rule_pid;
	u_int8_t        dir;
	u_int8_t        pad[3];
};

char anchor[PF_ANCHOR_NAME_SIZE];
#define FWKNOP_ANCHOR_NAME "fwknop/"
#define MAX_PFRULE_LENGTH 1024

/*
 * length and content of the "decrypted" message
 */
struct message {
	u_int32_t length;
	char     *content;
};

struct md5_item {
	u_int32_t timestamp;
	struct in_addr address;
	char *value;
	SLIST_ENTRY(md5_item) next;
};


/*
 * Configurable bounds
 */
u_int32_t max_length_md5_list = 1000;

/* 
 * 10 seconds between knock and real connection:
 * should be enough */
u_int32_t knock_pfrule_max_age = 10;
#define   MIN_PFRULE_AGE       1

/* 
 * 300 seconds to connect: one has to take into account
 * clock discrepancies.
 */
u_int32_t knock_delay          = 300;
#define MIN_KNOCK_DELAY        1


#define MAX_CONFIG_LINE        1024
#define MAX_IP_INFO_LENGTH      512
#define PASS_RULE_STRING "echo 'pass in proto %s from %s to any port %s'\
 | pfctl -a '%s%s' -mf -"
#define DEL_RULE_STRING "pfctl -a %s%s -F all\n"


/*
 * functions in fwknopd.c
 */
void packet_callback(u_char*, 
		     const struct pcap_pkthdr*, 
		     const u_char*);

int  parse_config(char *);

int  parse_message(struct in_addr, 
		   u_short *, 
		   u_int16_t *, 
		   char *,
		   u_int32_t *,
		   const u_char *, 
		   u_int32_t);

int  check_src(struct in_addr);

int  open_door(char *, 
	       char *, 
	       char *, 
	       char *);

int  check_age(u_int32_t );

int  cleanup_old();

int  verify_md5(u_char[] , 
		char *[]);

int  check_md5(u_int32_t, 
	       in_addr_t, 
	       char *);

struct message rsa_decrypt(const char *, 
			   u_int32_t);

extern int errno;
