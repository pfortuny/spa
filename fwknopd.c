/* fwknopd.c
 *
 * (c) 2008 Pedro Fortuny Ayuso
 * This software is in the public domain,  so
 * you can and may do with it whatever you like.    
 */

/* 
 * fwknopd: Single Packet Acceptance server, for OpenBSD (pf packet filter).
 * 
 * Once running, the server keeps listening passively for a UDP packet on
 * a specific port. If the packet matches a certain expression in a certain
 * timeframe, then the server opens a port for a specific source address for some time,
 * (and after that time closes the port).
 * This allows for 'listening' servers with no open ports in a firewall.
 *
 * This file is a first exercise on OpenSSL.
 *
 * Right now it only compiles on OpenBSD.
 */

#include <err.h>
#include <stdio.h>
#include <string.h>
#include <pcap.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/pfvar.h>
#include <openssl/ssl.h>
#include <openssl/md5.h>
#include "b64.h"
#include "fwknopd.h"



/* default values for main parameters */
char *interface   = "pflog0";
char *config_file = "/etc/fwknop/fwknop.conf";

/* keys_path probably unused */
char *keys_path   = "/etc/fwknop/keys/";

/* list of md5 signatures, this is an OpenBSD utility */
SLIST_HEAD(listhead, md5_item) md5_head;


/* 
 * fwknopd -i IF -c FILE
 */

int main(int argc, char *argv[]) {  
	extern char *interface, *config_file;
	char *options, *tag;
	char errbuf[PCAP_ERRBUF_SIZE] ;
	u_int32_t ch;
	pcap_t *PCAP_CONNECTION;

	/*
	 * see fwknopd.h:
	 * &md5_head is the head of a list of md5 sums of messages
	 * against which each message is checked 
	 * (no repeated messages are allowed) 
	 */
	SLIST_INIT(&md5_head);


	options      = "i:c:";
	/*
	 * options:                  
	 *  -i int                    
	 *     sniffing interface
	 *     default: pflog0
	 *
	 *  -c file                   
	 *     configuration file for the daemon
	 *     default: /etc/fwknop/fwknop.conf
	 */
	while((ch = getopt(argc,argv,options)) != -1) {
		switch(ch) {
		case 'i':
			interface = optarg;
			break;
		case 'c':
			config_file = optarg;
			break;
		}
	};
	warnx("Using:\nInterface: %s, Config: %s\n", interface, config_file);


	parse_config(config_file);

	/*
	 * create packet capture descriptor
	 *
	 * capture 1500 bytes at most,
	 * in promiscuous mode,
	 * with a timeout of 100ms
	 * using error buffer errbuf
	 * 
	 */
	if((PCAP_CONNECTION = 
	    pcap_open_live(interface, 1500, 1, 100, errbuf)) == NULL) {
	        errx(1, "Unable to use interface %s.\n Error: ", interface, errbuf);
		exit(errno);
	}

	/*
	 * for each [1] collected packet by PCAP_CONNECTION, run packet_callback
	 * 
	 * tag has no use as of now.
	 */
	for(;;) {
		pcap_loop(PCAP_CONNECTION, 1, &packet_callback, tag);
	}


	exit(0);
}


/*
 * Parse the configuration file. This is standard field = value list parsing.
 * There must be hundreds of utilities to do this dirty job.
 */
int parse_config(char *config_file) {

        /*
	 * See the documentation for the details, but in short:
	 *
	 * valid_sources is a linked list of IP addresses. If a packet comes
	 *               from one of them, it is taken into account and  processed.
	 *               Otherwise, it is discarded.
	 * knock_delay   maximum time difference between the packet's timestamp
	 *               and now (used to prevent packets from the future or too old).
	 *               May serve as anti-replay filter
	 * knock_pfrule_max_age Time of life for any anchor. They are removed after
	 *                      this time has elapsed
	 * anchor        For each valid message, we create an anchor (man 5 pf.conf)
	 *               holding the corresponding 'pass in' rule
	 * present_addr  The address being read from the config file
	 * config_string The line being read from the config file
	 * sources       Pointer to a string containing valid IP addresses. Will
	 *               be parsed using strsep (quite handy, btw)
	 * CONFIG        The configuration FILE*
	 *
	 */
	extern struct in_addr_item *valid_sources;
	extern u_int32_t knock_delay, knock_pfrule_max_age;
	struct in_addr_item *last_valid;
	extern char anchor[PF_ANCHOR_NAME_SIZE];
	char *present_addr,
	     *config_string;
	char **sources;
	FILE *CONFIG;


	/* temporary variables */
	u_char temp;
	int templen = 0;
	struct in_addr addr;

	if((CONFIG = fopen(config_file, "r")) == NULL) {
		err(1, "%s\n", config_file);
	}

	config_string = (char *)(calloc (MAX_CONFIG_LINE, sizeof(char)));
	if(config_string == NULL) {
		err(1, "calloc");
	}
  
	/* 
	 * default value for anchors 
	 */
	strlcpy(&anchor[0], FWKNOP_ANCHOR_NAME, strlen(FWKNOP_ANCHOR_NAME)+1);

	/*
	 * valid_sources ought to be a LIST MACRO... as of OpenBSD
	 */
	valid_sources = 
		(struct in_addr_item *)calloc(1,sizeof(struct in_addr_item));

	if(valid_sources != NULL) {
		inet_aton("0.0.0.0",&(valid_sources->address));
		valid_sources->next = NULL;
		last_valid = valid_sources;
	} else {
		err(1, "calloc");
	}

	/*
	 * Initialize the list of IP source addresses
	 */
	sources = (char **)(calloc(1,sizeof(char *)));
	if(sources == NULL) {
		err(1, "calloc");
	}

	/* 
	 * parsing loop: fgets returns NULL if either error or FEOF
	 *               lines are assumed at most 1500 chars long
	 */
	while(fgets(config_string, MAX_CONFIG_LINE, CONFIG)) {
    
		/* take trailing funny symbols away */
		while(strlen(config_string) > 0 &&
		      ((*(config_string + strlen(config_string) - 1) >= 'z') ||
		       (*(config_string + strlen(config_string) - 1) <= ' ')))
			*(config_string + strlen(config_string) - 1) = '\0';

		/*
		 * Source: (allowed sources), comma-separated IPs (net addresses
		 *         allowed: 192.168.0.0 for 8, 16, 24 bit long masks).
		 */
		if((strstr(config_string, "Source:")) == config_string) {
			*sources = config_string + 7;

			while((present_addr = strsep(sources,",")) != NULL) {
				while(*present_addr == ' ')
					present_addr++;

				templen = strlen(present_addr) - 1;
				
				/*
				 * Take away non [0-9.] characters.
				 */
				while(templen > 0 && 
				      ((temp = *(present_addr + templen)) < '0' ||
				       temp > '9') &&
				      (temp != '.'))
					*(present_addr + templen--) = '\0';
				/*
				 * Add address to valid source list
				 * and update *last_valid
				 */
				if(inet_aton(present_addr, &addr)) {
					last_valid->address = addr;
#ifdef DEBUG
					warnx("Added valid source: %s\n", 
					      inet_ntoa(addr));
#endif
					last_valid->next = 
						(struct in_addr_item *)
						calloc(1, 
						       sizeof(struct in_addr_item));
					if(last_valid->next == NULL) {
						errx(1, "calloc");
					}
					last_valid = last_valid->next;
					inet_aton("255.255.255.255", 
						  &(last_valid->address));
				}
			}
		}


		/*
		 * Anchor: (name of the pf anchor holding our rules).
		 * Usually Anchor: fwknop/
		 */
		if((strstr(config_string, "Anchor:")) == config_string) {
			char *temp_str = config_string + 7;
			while(*(++temp_str) == ' ') {}
			strncpy(&anchor[0], temp_str, PF_ANCHOR_NAME_SIZE );
		}


		/*
		 * Delay: (how old --or in the future-- can a packet be)
		 *        this deals with clock differences between computers
		 *        and may serve as anti-replay filter
		 */
		if((strstr(config_string, "Delay:")) == config_string) {
			char *temp_str = config_string + 6;
			while(*(++temp_str) == ' ') {}

			knock_delay = 
			  ((knock_delay = atoi(temp_str)) < MIN_KNOCK_DELAY ) ?
				MIN_KNOCK_DELAY : knock_delay;
		}


		/*
		 * Age: how long to keep pf rules active, in seconds (after this
		 *      time, the anchor is deleted and the connection will fail)
		 */
		if((strstr(config_string, "Age:")) == config_string) {
			char *temp_str = config_string + 4;
			while(*(++temp_str) == ' ') {}
			
			knock_pfrule_max_age = 
			  ((knock_pfrule_max_age = atoi(temp_str)) < 
			   MIN_KNOCK_DELAY ) ?
				MIN_KNOCK_DELAY : knock_pfrule_max_age;
		}
	}

	return(0);
}


/*
 * packet_callback: the true engine. After receiving a packet, check its source,
 *                  parse it, etc...
 *                   
 */
void packet_callback(u_char *tag, const struct pcap_pkthdr *header, 
		     const u_char *packet) {

	/*
	 * len:     remaining length of packet 
	 *          (from which we discount headers as we parse them)
	 * ll_size: real size of ethernet header (depends on pf/not pf)
	 */
	bpf_u_int32 caplen;
	u_int32_t tv_sec, tv_usec, 
		len, ll_size, 
		size_ip, size_udp,
		timestamp;
	u_int16_t tos;
	u_char* payload;
	u_short *ip_p;
	u_int16_t *port;
	char ether_hdr[14];
	struct in_addr real_src_ip;

	extern char *interface;
	struct etherhdr *ethernet;
	struct iphdr *ip = (struct iphdr *)calloc(1, sizeof(struct iphdr));
	struct udphdr *udp = (struct udphdr *)calloc(1, sizeof(struct udphdr));

	/* 
	 * pflogheader: NOT a standard ethernet header, but used by default
	 */
	struct pfloghdr *pfhdr;
	int pflog = 1;

	char *user;

	/* 
	 * delete old rules from pf anchor's
	 */
	cleanup_old();

	/*
	 * parse Ethernet + TCP/IP header
	 */
	tos     = ether_hdr[12]*256 + ether_hdr[13];
	tv_sec  = header->ts.tv_sec;
	tv_usec = header->ts.tv_usec;
	caplen  = header->caplen;
	len     = (u_int32_t )header->len;
  
	/*
	 * ethernet header length
	 */
	if(strncmp("pflog", interface, 5) == 0){
		warnx("Interface is pflog-like\n");
		ll_size = sizeof(struct pfloghdr);
		pfhdr   = (struct pfloghdr *)(packet);
		ll_size = BPF_WORDALIGN(pfhdr->length);
	} else {
		ll_size = SIZE_ETHERNET;
		pflog = 0;
	}

	/* get IP header */
	if (len >= ll_size) {
		ethernet = (struct etherhdr *)(packet);
		ip = (struct iphdr *)(packet + ll_size);
		len -= ll_size;
	} else {
		warnx("Packet has no valid ethernet header"
		      "is only %u bytes long\n", len);
		return;
	}
  
	/*
	 * parse IP header, get TCP/UDP header
	 */
	real_src_ip = ip->ip_src;
	size_ip = IP_HL(ip)*4;
	if (size_ip < 20) {
		warnx("Invalid IP header length: %u bytes\n", size_ip); 
		return; 
	}
	/* protocol not UDP, return */
	if (ip->ip_p != IPPROTO_UDP) {
		return;
	}
	if(check_src(real_src_ip) == 1){
		warnx("Source IP: %s ALLOWED!\n", inet_ntoa(real_src_ip));
#ifdef DEBUG
		warnx("Dest.  IP: %s\n", inet_ntoa(ip->ip_dst));
#endif
	} else {
		warnx("Source IP: %s FORBIDDEN!\n", inet_ntoa(real_src_ip));
		return;
	}
  
	/* 
	 * parse UDP header (we only allow UDP connections by now)
	 */
	if (len >= size_ip) {
		udp = (struct udphdr*)(packet + ll_size + size_ip);
		len -= size_ip;
	} else {
		warnx("Invalid UDP packet, its IP header is too short"
		      ", %u bytes\n", len);
		return;
	}
	size_udp = sizeof(struct udphdr);

	/*
	 * payload
	 */
	if (len >= size_udp) {
		payload = (u_char*)(packet + ll_size + size_ip + size_udp);
		len -= size_udp;
	} else {
		warnx("* Invalid UDP header: length %u bytes\n", len);
		return;
	}
  
#ifdef DEBUG
	warnx("Ports etc [%u], [%u]\n", 
	      swap16(udp->uh_sport), swap16(udp->uh_dport));
#endif
	if((ip_p = calloc(1,1)) == NULL) {
		err(1,"calloc");
	} ;
	if((port = calloc(1,2)) == NULL) {
		err(1,"calloc");
	}
	if((user = calloc(1,64)) == NULL) {
		err(1,"calloc");
	}

	time(&timestamp);
	parse_message(real_src_ip, ip_p, port, user, &timestamp, payload,len);

	return;
}



/* The decrypted message must be as follows:
 *  0  16 randon BYTES
 *  1  base64(username)
 *  2  timestamp
 *  3  software version
 *  4  mode = 1 (access mode, we do not implement command mode)
 *  5  base64("IP, proto, port")
 *  6  MD5 sum
 *    -->with a ':' between each of the fields
 *
 * The encryption is private_rsa_encrypt(message), we have the public key
 *
 * (*message) = encrypted message
 */

int parse_message(struct in_addr real_ip,
		  u_short *ip_p, u_int16_t *port, char *user, 
		  u_int32_t *timestamp, const u_char *message, u_int32_t len){
	char *params[7];
	char **text;
	char *username ;
	BIGNUM *digest;
	u_int32_t time ;
	int i, l;
	u_char *md5 , *md5_hex;
	char *IP;
	char *s_ip, *d_port, *proto;
	struct message decrypted;
	decrypted.content = "\0";
	decrypted = rsa_decrypt(message, len);

	if(decrypted.length == -1) {
		warnx("Error while decrypting the message\n");
		return(-1);
	}

#ifdef DEBUG
	warnx("Veamos el mensaje.... [%s]\n", decrypted.content);
#endif
  

	/* copypaste from man 9 STRSEP */
	for (text = params; text < &params[9] &&
		     (*text = strsep(&(decrypted.content), ":")) != NULL;) {
		if (**text != '\0')
			text++;
	}

	for(i=0; i<7 && params[i] != NULL ; i++){
#ifdef DEBUG
		warnx("Parametro %d: [%s]\n", i, params[i]);
#endif
	}
  
	/*
	 * params[0]: 16 NUMBERS (bin) 
	 */
	if(strlen(params[0]) != 16) return(0);
	l = 16 + 1; /* : */

	/* 
	 * params[1]: b64encode(username)
	 */
	username = calloc(1,513);
	b64decode(params[1], username);
	l += strlen(params[1]) + 1; /* : */ 

	/*
	 * params[2]: timestamp
	 */
	time = atoi(params[2]);
	if(check_age(time) != 1) {
		warnx("Packet too old, not accepted\n");
		return(0);
	}
	l += strlen(params[2]) + 1; /* : */

	/* params[4] and params[5] are obviated in this version */
	l += strlen("1:1:");

	/*
	 * params[5]: IP info:
	 *            IPv4 address in dot notation
	 *            proto
	 *            port
	 * in a b64encode(comma-separated string)
	 */
	IP = calloc(1,MAX_IP_INFO_LENGTH);
	b64decode(params[5], IP);
	s_ip     = calloc(1,32);
	d_port   = calloc(1,8);
	proto    = calloc(1,8);
	sscanf(IP, "%16[0-9.],%3c,%5[0-9]", s_ip, proto, d_port);
	l += strlen(params[5]);
  
	/* 
	 * params[6]: md5 sum
	 */
	md5 = params[6];
	digest = BN_new();
	BN_bin2bn(md5,16, digest);
	md5_hex = BN_bn2hex(digest);
 
	/* verify md5's correctness */
	if(verify_md5(md5, params) != 1)
		return(0);

	/* verify packet's uniqueness, timestamp, etc... */
	if(check_md5(time, real_ip.s_addr, md5_hex) == -1) {
		return(0);
	};

	/* s_ip is now useless: must use real_ip, for NAT reasons */
	strncpy(s_ip, inet_ntoa(real_ip), 32);
  
	/* if we are here, the packet is good */
	open_door(s_ip, d_port, proto, md5_hex);
	warnx("user:  [%s], IP:    [%s], proto: [%s], port:  [%s], "
	      "time:  [%d], \n",
	      username, s_ip, proto, d_port, time);  
	warnx("[Door open]\n");
	return(1);
}



/*
 * Delete rotten entries from pf's cache. The entry SLIST starts at md5_head
 */
int cleanup_old(){
	struct md5_item *np;
	extern u_int32_t knock_pfrule_max_age;
	extern char anchor[PF_ANCHOR_NAME_SIZE];
	char **pfctl_call;

	if((pfctl_call = calloc(1,sizeof(char*))) == NULL) {
		errx(1,"calloc");
	}
	if((np = calloc(1, sizeof(struct md5_item))) == NULL) {
		errx(1,"calloc");
	}

	if(SLIST_EMPTY(&md5_head)) 
		return(0);

	SLIST_FOREACH(np, &md5_head, next) {
		if(abs(np->timestamp - time(NULL)) > knock_pfrule_max_age) {
			asprintf(pfctl_call,
				 DEL_RULE_STRING, 
				 &anchor[0], 
				 np->value); 
			warnx("%s\n", *pfctl_call);
			SLIST_REMOVE(&md5_head, np, md5_item, next);
			if(system(*pfctl_call) != 0){
				warnx("Problems deleting pf rule as\n%s\n", 
				      *pfctl_call);
				return(-1);
			}
		}
	}

	return(0);
}


/*
 * check that the packet's md5 sum is not cached, otherwise reject the knock.
 *
 * This function also cleans up the oldest entry if there are more than
 * max_length_md5_list in fwknop's pf anchor.
 */
int check_md5(u_int32_t timestamp, in_addr_t ip, char *md5){
	struct md5_item *present_value, *np; 
	struct in_addr ip_addr;
	char **warning;
	u_int i = 1;
	ip_addr.s_addr = ip;
	present_value = calloc(1, sizeof(struct md5_item));
	present_value->timestamp = timestamp;
	present_value->address.s_addr   = ip;
	present_value->value = calloc(1, 33);
	strlcpy(present_value->value, md5, MD5_DIGEST_LENGTH + 1);
	
	/*
	 * insert in list and return if no cached values yet
	 */
	if(SLIST_EMPTY(&md5_head)) {
		SLIST_INSERT_HEAD(&md5_head, present_value, next);
		return(0);
	}

	SLIST_FOREACH(np, &md5_head, next) {
		
		/* 
		 * source IP is cached?
		 */
		if(np->address.s_addr == present_value->address.s_addr) {
			warning = calloc(1,sizeof(char *));
			asprintf(warning, 
				 "Packet from same address %s too soon"
				 ", rejecting\n", 
				 inet_ntoa(ip_addr));
			warnx(*warning);
			return(-1);
		}

		/*
		 * md5 sum is cached?
		 */
		if((strcmp(np->value, present_value->value) == 0)) {
			warnx("MD5 already used. Packet rejected.\n");
			return(-1);
		}
		i++;
	}

	/*
	 * remove first cached value if too many cached
	 *  (>max_length_md5_list, which is configurable)
	 */ 
	if(i>max_length_md5_list) {
		SLIST_REMOVE_HEAD(&md5_head, next);
	}

	/*
	 * if we have arrived here, cache present md5_item
	 */
	SLIST_INSERT_HEAD(&md5_head, present_value, next);

	return(0);
}



/*
 * decrypt the message, should be obvious
 */
struct message rsa_decrypt(const char *payload, u_int32_t len) {
	FILE *key_file ; 
	RSA *rsa;
	BIGNUM *temp, *in, *out, *out2;
	char *decrypted;
	struct message answer;
	if ((rsa = RSA_new()) == NULL)
		warnx("calloc");
	if ((rsa->n = BN_new()) == NULL)
		warnx("calloc");
	if ((rsa->e = BN_new()) == NULL)
		warnx("calloc");
	/*
	 * this path should be customizable.
	 * Read public RSA key
	 */
	/* bug bug bug !!!! */
	if((key_file = fopen("/etc/fwknop/id_rsa_pub.pem","r"))==NULL){
	        errx(1,"Unable to open id_rsa_pub.pm\n");
	};
	rsa = PEM_read_RSA_PUBKEY(key_file,&rsa,NULL,"");
	if (rsa == NULL) {
	        errx(1,"Unable to read public key\n");
	}
	fclose(key_file);

	/*
	 * typical RSA preparation and decryption
	 */
	in = BN_new();
	temp = BN_new();
	BN_bin2bn(payload, len, temp);
	out = BN_new();
	out2 = BN_new();
	decrypted = calloc(1,RSA_size(rsa));
	answer.length = RSA_public_decrypt(len,payload,decrypted, 
					   rsa, RSA_PKCS1_PADDING);
	/*
	 * store decrypted message only on success
	 */
	if (answer.length != -1) {
		answer.content = decrypted;
	}

	return(answer);
}



/*
 * physically open the door
 */
int open_door(char *IP, char  *port, char *proto, char *md5){
	char *new_rule;
	int dev;
	char *anchorname;
	extern char anchor[PF_ANCHOR_NAME_SIZE];
	struct pfioc_ruleset  prs;
	
	/*
	 * we are using ioctl to check whether we can tell the rule to pf
	 */
	dev = open("/dev/pf", O_RDWR);
	
	/*
	 * full anchor name is 
	 * anchor . md5
	 * where anchor is a configurable value, defaulting to 'fwknop/'
	 * and md5 is the hex value of the message md5 sum
	 */
	anchorname = calloc(1, PF_ANCHOR_NAME_SIZE+1);
	snprintf(anchorname, PF_ANCHOR_NAME_SIZE, "%s%s\n", &anchor[0], md5);
	
	memset(&prs, 0, sizeof(prs));
	strlcpy(prs.path, anchorname, sizeof(prs.path));
	if (ioctl(dev, DIOCGETRULESETS, &prs)) {
		if (errno != EINVAL) {
			/*
			 * should not happen: rule exists, won't rewrite
			 */
			warn("Repeated MD5!!!!");
			return (-1);
		}
	}

	if((new_rule = calloc(1, MAX_PFRULE_LENGTH)) == NULL){
		errx(1, "calloc");
	}
  
	if((snprintf(new_rule, 
		     MAX_PFRULE_LENGTH,
		     PASS_RULE_STRING, 
		     proto, 
		     IP,  
		     port, 
		     &anchor[0], 
		     md5)) > MAX_PFRULE_LENGTH) {
		warn("Wrong Data: [%s]\n", new_rule);
		return(-1);
	}
	
	warnx("Sending [%s]\n", new_rule);
	system(new_rule);

	return(0);
}


/*
 * check validity of source IP. Notice that, by default, all sources
 * are allowed unless there is (explicitly) a Source: statement in
 * the configuation file.
 */
int check_src(struct in_addr src) {
	extern struct in_addr_item *valid_sources;
	struct in_addr_item *source;
	
	/*
	 * should be a SLIST
	 */
	source = valid_sources;

	/*
	 * return 1 if the source address is allowed
	 */
	while(source != NULL) {
		if(inet_netof((source->address)) == (source->address).s_addr) {
			if((source->address).s_addr == 0) 
				return(1);
			if(inet_netof(src) == (source->address).s_addr) 
				return(1);
		} else {
			if((source->address).s_addr == src.s_addr) 
				return(1);
		}
		source = source->next;
	}

	/*
	 * else return 0
	 */
	return(0);
}


/*
 * age: there are many reasons why a packet can turn out late or in the
 * future, mainly clock incoordination; ther is a knock_delay margin for this
 */
int check_age(u_int32_t timestamp) {
	u_int32_t now = 0;
	extern u_int32_t knock_delay;

	time(&now);
	warnx("Delay: [%d]\n", abs(timestamp - now));
	
	if(abs(timestamp - now) > knock_delay){
		return(0);
	}
	return(1);

}


/*
 * compute message's md5 and check it against messages 'said' md5. This
 * prevents fake md5 usage (which might be used to get tinker with the encryption
 * algorithm and get a 'true' message)
 */
int verify_md5(u_char md5[], char *params[]){
	MD5_CTX *Context; 
	char **message_text;
	u_char local_md5[MD5_DIGEST_LENGTH];
	int i;

	/*
	 * typical MD5 environment setup
	 */
	if((Context = calloc(1, sizeof(MD5_CTX))) == NULL){
		errx(1,"calloc");
	};
	if((message_text = calloc(1, sizeof(char *))) == NULL) {
		errx(1,"calloc");
	}
	
	/* 
	 * params[3] and params[4] are useless in the present version
	 */
	asprintf(message_text, "%s:%s:%s:1:1:%s",
		 params[0], params[1], params[2], params[5]);
	MD5_Init(Context);
	MD5_Update(Context, *message_text, strlen(*message_text));
	MD5_Final(local_md5, Context);
	
	/* 
	 * compare both md5 sums, byte by byte
	 */
	for(i=0; i<MD5_DIGEST_LENGTH; i++){
		if(local_md5[i] != md5[i]) {
			warnx("Packet with wrong md5!!! (details follow).\n");
			warnx("\tOur md5 is [%s]\n, theirs is [%s]\n", 
			      &local_md5[0], &md5[0]);
			return(0);
		}
	}

	return(1);
}

