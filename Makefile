DEBUG  = -DDEBUG
WARN   = -Wall

CC     = gcc -g $(WARN) $(DEBUG)
default: b64 fwknopd.c fwknopd.h
	$(CC) fwknopd.c -lpcap -lssl -lcrypto b64.o -o spad

client: b64 fwknop.c fwknop.h
	$(CC) fwknop.c -lpcap -lssl -lcrypto b64.o -o spa

b64:
	$(CC) b64.c -c

tar:
	tar cvf spa.tar b64.c b64.h fwknop.c fwknop.h fwknopd.c fwknopd.h Makefile

clean:
	rm -f *~ *.o spad spa