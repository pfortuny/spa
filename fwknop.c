#include<stdio.h>
#include<unistd.h>
#include<err.h>
#include<string.h>
#include<pcap.h>
/*#include<md5.h>*/
#include<netdb.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<openssl/rsa.h>
#include"fwknop.h"
#include"b64.h"

int D_PORT       = 5151;
int delay        =    0;
char *username   = "root";
char *proto      = "tcp";
char *port       = "22";
char *IP         = "127.0.0.1";
char **dumpfile  = NULL;

int main(int argc, char *argv[]) {
        char *rand, *version, *mode, *net, *message;
        char *user64, *net64, *temp_str;
        extern char *optarg;
        extern int optind;
        in_addr_t dest_ip = -1;
        MD5_CTX *Context;
        u_char md5[MD5_DIGEST_LENGTH];
        FILE *key_file, *rand_dev ; 
        RSA *rsa;
        int i;
        BIGNUM *temp_bn, *in;
        char *encrypted, *plain;
        char ch;
        char *options = "u:P:p:s:d:f:";
        /* options:                  */
        /* -u str                    */
        /*    username (def => blah) */
        /* -P proto                  */
        /*    protocol (tcp/udp)     */
        /* -p port                   */
        /*    port (1 < p < 65535)   */
        /* -s IP                     */
        /*    source IP in network   */
        /*    format: a.b.c.d        */
        /*    (IPv4 by now)          */
        while((ch = getopt(argc,argv,options)) != -1) {
                switch(ch) {
                case 'u':
                        username = optarg;
                        break;
                case 'P':
                        proto    = optarg;
                        break;
                case 'p':
                        port     = optarg;
                        break;
                case 's':
                        IP       = optarg;
                        break;
                case 'd':
                        delay = atoi(optarg);
                        break;
                case 'f':
                        dumpfile = calloc(1, sizeof(char *));
                        asprintf(dumpfile, "%s", optarg);
                }
        };
        argc -= optind;
        argv += optind;
        if(*argv != NULL) {
                /* next cast is a cheat */
                inet_aton(*argv, (struct in_addr *) &dest_ip);
        }
        if(dest_ip == -1) {
                warn("Invalid IP %s\n", *argv);
                exit(-1);
        }
        mode = version = "1";
        user64 = calloc(1,512);
        net    = calloc(1,512);
        net64  = calloc(1,512);
        b64encode(username, strlen(username), user64, 512);
        strncat(net, IP, 32);
        strncat(net, ",", 1);
        strncat(net, proto, 32);
        strncat(net, ",", 1);
        strncat(net, port, 32);
        b64encode(net, strlen(net), net64, 512);
        message = calloc(1,512);
        rand = calloc(1,17);
        rand_dev = fopen("/dev/urandom","r");
        for(i=0;i<16;i++){
                rand[i] = getc(rand_dev) % 10 + '0';
        }
        fclose(rand_dev);
        strncat(message, rand, 16);
        strncat(message, ":", 1);
        strncat(message, user64, 64);
        strncat(message, ":", 1);
        temp_str = calloc(1,32);
        snprintf(temp_str, 32, "%d", (int )time(NULL));
        if(delay != 0) {sleep(delay);}
        strncat(message, temp_str, 32);
        strncat(message, ":1:1:", 5);
        strncat(message, net64, 128);
        Context = (MD5_CTX *)calloc(1, sizeof(MD5_CTX));
        MD5_Init(Context);
        MD5_Update(Context, message, strlen(message));
        MD5_Final(md5, Context);
        strncat(message, ":", 1);
        strncat(message, (char *)md5, 128);
        if ((rsa = RSA_new()) == NULL)
                warnx("key_new: RSA_new failed");
        if ((rsa->n = BN_new()) == NULL)
                warnx("key_new: BN_new failed");
        if ((rsa->e = BN_new()) == NULL)
                warnx("key_new: BN_new failed");
        key_file = fopen("id_rsa","r");
        rsa = PEM_read_RSAPrivateKey(key_file,&rsa, NULL, "");
        fclose(key_file);
        in = BN_new();
        temp_bn = BN_new();
        plain = calloc(1,512);
        in = BN_bin2bn((u_char *)message, 512, in);
        encrypted = calloc(1,512);
        RSA_private_encrypt(strlen(message),(u_char *)message,(u_char *)encrypted, rsa, RSA_PKCS1_PADDING);
        BN_bin2bn((u_char*) encrypted, 512, temp_bn);
        send_message(dest_ip, D_PORT, encrypted, 512);
        if(dumpfile != NULL)
                dump(dumpfile, encrypted, 512);
        exit(0);
}


void dump(char **file, char *w, int l) {
        FILE *out;
        if((out = fopen(*file,"w")) == NULL) {
                warn("Unable to create dump file [%s]\n", *file);
                return;
        }
        while(l>0) {
                fprintf(out,"%c",*(w++));
                l--;
        }
        fclose(out);
        return;
}

int send_message(in_addr_t dst, u_int d_port, char *message, int len) {
        int my_socket;
        struct hostent *dest;
        struct sockaddr_in server;
        struct in_addr *s_address;
        char *DEST;
        s_address = calloc(1,sizeof(struct in_addr));
        DEST = calloc(1024, sizeof(char));
        bzero((char *)&server, sizeof(server));
        s_address->s_addr = dst;
        dest = calloc(1, sizeof(struct hostent)); 
        dest = gethostbyaddr((char*)s_address, sizeof(struct in_addr), AF_INET);
        /*    dest = gethostbyaddr("192.168.1.1", sizeof(struct in_addr), AF_INET);*/
        if(dest == NULL) {
                printf("Hi, destination not found!\n");
                dest = calloc(1, sizeof(struct hostent));
                dest->h_name = calloc(1024,sizeof(char ));
                strcpy(dest->h_name, "no.host.here"); 
                dest->h_aliases = NULL;
                dest->h_addrtype = AF_INET;
                dest->h_length = sizeof(struct in_addr);
                dest->h_addr_list = (char **)calloc(2, dest->h_length);
                dest->h_addr = calloc(1,dest->h_length);
                *dest->h_addr_list = dest->h_addr;
                memcpy((char *)dest->h_addr, (char *)&dst, 4);
        }
        bcopy(dest->h_addr,(char *)&server.sin_addr,4);
        server.sin_family = AF_INET; 
        server.sin_port   = htons(D_PORT);
        while(!(my_socket = socket(AF_INET, SOCK_DGRAM, 0))) {}
        /* next cast is another cheat? */
        connect(my_socket, (struct sockaddr *)&server, sizeof(server));
        send(my_socket, message, len, 0);
        close(my_socket);
        return(0);
}



